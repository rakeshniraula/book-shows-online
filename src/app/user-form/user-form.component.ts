import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, EmailValidator, FormBuilder } from '@angular/forms'
import { EmailValidators } from '../common/validators/email.validators';
import { PasswordValidators } from '../common/validators/password.validators';
import { ActivatedRoute, Router } from '@angular/router';
import { UserSignupService } from '../services/user-signup.service';
import { User } from '../helpers/User';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  signupForm: FormGroup;
  passwordForm: FormGroup;

  submitted = false;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userSignupService: UserSignupService,) {}

  ngOnInit() {
    this.passwordForm = this.fb.group({
      password: ['',
        Validators.compose([Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)])],
      confirm_password: ['',
        Validators.compose([Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)])]
    }, {
        validator: PasswordValidators.passwordMatchValidator
      });
    this.signupForm = this.fb.group({
      username: ['',
        Validators.compose([Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)])],
      email: ['',
        Validators.compose([Validators.required,
        Validators.email,
        Validators.maxLength(50)]),
        EmailValidators.unique],
      passwordFormGroup: this.passwordForm
    });
  }

  passwordMatchValidator(g: FormGroup) {
    return g.get('password').value === g.get('passwordConfirm').value
      ? null : { 'notMatch': true };
  }

  user:User;
  onSubmit() {
    console.log(event)
    this.submitted = true;
    //stop here if signup form is invalid
    if (this.signupForm.invalid) {
      console.log(this.signupForm.invalid)
      return;
    }

    console.log("posting...")
    this.user = {
      username: this.username.value,
      email: this.email.value,
      password: this.password.value,
      phone: 9,
      gender: "male"
    };
    this.userSignupService.registerUser(this.user);
  }

  // form = new FormGroup({
  //   username: new FormControl('', [
  //     Validators.required,
  //     Validators.minLength(6),
  //     Validators.maxLength(30)
  //   ]),
  //   email: new FormControl('', [
  //     Validators.required,
  //     Validators.email,
  //     Validators.maxLength(50),
  //     EmailValidators.cannotContainSpace
  //   ], EmailValidators.unique),
  //   password: new FormControl('', [
  //     Validators.required,
  //     Validators.minLength(8),
  //     Validators.maxLength(30)
  //   ])
  // });

  get username() {
    return this.signupForm.get('username');
  }

  get email() {
    return this.signupForm.get('email');
  }

  get password() {
    return this.passwordForm.get('password');
  }

  get confirm_password() {
    return this.passwordForm.get('confirm_password');
  }

}
