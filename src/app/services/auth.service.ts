import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs'
import { map, catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = "http://Localhost/api/main/"

  constructor(private http: HttpClient) {
    let token = localStorage.getItem('token');
    if (token) {

    }
  }

  login(email: string, password: string) {
    return this.http.post<any>(this.url + 'login.php', {
      'email': email,
      'password': password
    }).pipe(map(user => {

      if (user && user.token) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
      }
      return user;
    }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }


}
