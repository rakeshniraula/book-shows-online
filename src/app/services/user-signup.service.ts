import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../helpers/User';
import { JsonPipe } from '@angular/common';
import { map, catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class UserSignupService {

  private url = "http://Localhost/api/main/";

  private register = 'register.php';

  constructor(private http: HttpClient) { }

  getUrl() {
    return this.url;
  }

  registerUser(user: User) {
    // console.log("user: " + JSON.stringify(user))
    return this.http.post(this.url + this.register, user)
    .subscribe(
      response =>{
        console.log(response);
      });
  }
}
