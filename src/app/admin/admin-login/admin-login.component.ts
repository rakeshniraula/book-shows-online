import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  adminLogin: FormGroup;

  constructor(fb: FormBuilder) {
    this.adminLogin = fb.group({
      email: ['',
        Validators.compose([Validators.required,
        Validators.maxLength(50)])
      ],
      password: ['',
        Validators.compose([Validators.required,
        Validators.minLength(8),
        Validators.maxLength(30)])]
    })
  }

  ngOnInit() {
  }

}
