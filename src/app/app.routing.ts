import { Routes, RouterModule } from "@angular/router";
import { UserLoginComponent } from "./user-login/user-login.component";
import { UserFormComponent } from "./user-form/user-form.component";
import { HomeComponent } from "./home/home.component";
import { AuthGuard } from "./guards/auth.guard";
import { AdminLoginComponent } from "./admin/admin-login/admin-login.component";



const appRoutes: Routes = [
  { path: 'admin', component: AdminLoginComponent, canActivate: [AuthGuard]},
  {  path: 'login', component: UserLoginComponent },
  { path: 'signup', component: UserFormComponent },
  { path: '', component: HomeComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
