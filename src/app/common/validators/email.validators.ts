import { AbstractControl, ValidationErrors } from "@angular/forms";

export class EmailValidators {
  static cannotContainSpace(control: AbstractControl): ValidationErrors | null {
    if ((control.value as string).indexOf(' ') > 0) {
      return { cannotContainSpace: true };
    }
    return null;
  }

  static unique(control: AbstractControl): Promise<ValidationErrors | null> {

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const email = control.value as string;

        return email == "rakeshhniraulaa@gmail.com" ? resolve({ unique: true }) : resolve(null);
      }, 2000);
    });
  }
}
