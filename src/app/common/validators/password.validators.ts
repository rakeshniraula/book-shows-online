import { ValidatorFn, AbstractControl, ValidationErrors, FormGroup } from "@angular/forms";

export class PasswordValidators {
  static match(signUpFormGroup: FormGroup): ValidationErrors | null {
    let pw = signUpFormGroup.controls.password.value as string;
    let confirm = signUpFormGroup.controls.confirm_password.value as string;

    return pw !== confirm ? { match: true } : null;
  }

  static passwordMatchValidator(control: AbstractControl) {
    const password: string = control.get('password').value;
    const confirmPassword: string = control.get('confirm_password').value;

    //compare if password matched
    if (password !== confirmPassword) {
      control.get('confirm_password').setErrors({ notMatch: true });
    } else {
      return null;
      // control.get('confirm_password').setErrors({ notMatch:null });
        // if (control.hasError('notMatch')) {
        //   delete control.errors['notMatch'];
        //   control.updateValueAndValidity();
        // }
    }
  }
}
