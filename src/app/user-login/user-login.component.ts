import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmailValidators } from '../common/validators/email.validators';
import { AuthService } from '../services/auth.service';
import { first } from 'rxjs/operators'
import { User } from '../helpers/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  loginForm: FormGroup;

  submitted = false;
  invalidLogin:boolean;

  user: User;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {

  }

  ngOnInit() {

    this.loginForm = this.fb.group({
      email: ['',
        Validators.compose([Validators.required,
        Validators.email,
        Validators.maxLength(50)])
      ],
      password: ['',
        Validators.compose([Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)])]
    })
  }

  onSubmit() {
    this.submitted = true;

    //stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.login()

  }

  login() {
    this.authService.login(this.email.value, this.password.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data);
          if (data['error'] == false) {
            this.user = {
              id: data['id'],
              username: data['username'],
              email: data['email'],
              password: data['password'],
              phone: data['phone'],
              gender: data['gender']
            };
            this.router.navigate(['/']);

          } else {
            this.invalidLogin = true;
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

}
